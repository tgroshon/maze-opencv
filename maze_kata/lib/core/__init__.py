from .maze import MalformedMazeError, Maze
from .solver import solve, DefaultStrategy
